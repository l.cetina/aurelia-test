import { FrameworkConfiguration } from 'aurelia-framework';

export function configure(config: FrameworkConfiguration) {
    //config.globalResources([]);
    config.globalResources([
        "./sidebar/sidebar.html",
        "./filter/filter-value-converter",
        "./highlight/highlight-value-converter",
        "./value-converters/first-letters-value-converter"
    ]);
}
