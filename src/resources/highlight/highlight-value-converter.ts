export class HighlightValueConverter {
    toView(value, query) {
      if (query === void 0 || query === null || query === "") {
        return value;
      }
      return value.replace(new RegExp(query,'gi'),`<mark>$&</mark>`);
    }
  }
