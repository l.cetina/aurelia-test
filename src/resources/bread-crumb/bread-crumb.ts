import { Router, NavigationInstruction } from 'aurelia-router';
import { autoinject, bindable } from 'aurelia-framework';
import {EventAggregator, Subscription} from 'aurelia-event-aggregator';
import _clone from 'lodash/clone';

@autoinject
export class BreadCrumbCustomElement {

    /**
     * Configuration object
     * @type {{seperator: 'htmlString'}}
     */
    @bindable config = {};
    subscription: Subscription;
    instructions: NavigationInstruction[];

    constructor(private router:Router, private eventAggregator:EventAggregator) {
        
    }

    attached() {
        this.subscription = this.eventAggregator.subscribe('router:navigation:success', this.refresh.bind(this));
        this.refresh();
    }

    detached() {
        this.subscription.dispose();
    }

  /**
   * Refresh the rendered widget
   */
    refresh() {
        const parentInstructions = this.getParentInstructions(this.router.currentInstruction);
        this.instructions = parentInstructions
            .slice(0, parentInstructions.length - 1)
            .concat(this.router.currentInstruction.getAllInstructions())
            .filter(instruction => instruction.config.breadcrumb && instruction.config.title);
    }

    navigateToRoute(instruction:NavigationInstruction) {
        const params = _clone(instruction.params);
        delete params.childRoute;
        instruction.router.navigateToRoute(instruction.config.name, params);
    }

    getParentInstructions(instruction:NavigationInstruction):NavigationInstruction[] {
        return instruction.parentInstruction ? this.getParentInstructions(instruction.parentInstruction).concat([instruction]) : [instruction];
    }
}
