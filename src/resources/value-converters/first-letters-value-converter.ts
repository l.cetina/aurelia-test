export class FirstLettersValueConverter {
    toView(value) {
        
        return this.getByCapitalLeters(value);
    }

    getByCapitalLeters(str:string) {
        const regex = /[A-Z]/g;
        let m;
        let result = "";
        while ((m = regex.exec(str)) !== null && result.length < 2) {

            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }


            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                result = result.concat(match);
            });
        }

        return result;
    }
}
